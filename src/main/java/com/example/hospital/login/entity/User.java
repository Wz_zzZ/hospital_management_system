package com.example.hospital.login.entity;

import com.sun.javafx.geom.transform.Identity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity  //表示是一个实体类，自动做映射
public class User {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)    //自动增长
    private  Integer id;



    private String password;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }



    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +

                ", password='" + password + '\'' +
                '}';
    }
}
