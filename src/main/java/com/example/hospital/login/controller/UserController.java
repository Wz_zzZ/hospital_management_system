package com.example.hospital.login.controller;

        import com.example.hospital.login.entity.User;
        import com.example.hospital.login.repository.UserRepository;

        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.stereotype.Controller;
        import org.springframework.web.bind.annotation.GetMapping;
        import org.springframework.web.bind.annotation.ResponseBody;
        import org.springframework.web.bind.annotation.RestController;

/**
 * author onlyqi
 *
 */
//@ResponseBody    //这个类的所有的方法返回的数据直接写给浏览器（如果是对象转为json数据）
//@Controller
@RestController
public class UserController {
    @Autowired
    private UserRepository userRepository;


    @GetMapping("/index")
    public String index(){
        User user = userRepository.getOne(1);
        return user.toString();
    }

    @GetMapping("/")
    public String login(){

        return "login1";
    }
}
