package com.example.hospital.dispensarypurchasesandsells.repository;

import com.example.hospital.dispensarypurchasesandsells.pojo.Medicine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicineRepo extends JpaRepository<Medicine,Integer> {


    Medicine findByName(String name);

    @Query(value ="select max(m.medicineId) from Medicine m " )
    int findMedicineId();

    @Query(value ="select m.name from Medicine m where m.name like concat('%',:kw,'%') " )
    List<String> findKeyWord(@Param("kw") String kw);
}
