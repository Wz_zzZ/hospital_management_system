package com.example.hospital.dispensarypurchasesandsells.repository;

import com.example.hospital.dispensarypurchasesandsells.pojo.InputRecord;
import com.example.hospital.dispensarypurchasesandsells.pojo.RecordInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InputRepository extends JpaRepository<InputRecord,Integer> {

    @Query(value = "select new com.example.hospital.dispensarypurchasesandsells.pojo.RecordInfo(m,i) from Medicine m,InputRecord i where i.medicineId = m.medicineId")
    List<RecordInfo> findRecordInfo();

    @Query(value ="select max(i.inputId) from InputRecord i " )
    int findInputId();


}
