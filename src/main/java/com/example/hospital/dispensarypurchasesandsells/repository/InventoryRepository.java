package com.example.hospital.dispensarypurchasesandsells.repository;

import com.example.hospital.dispensarypurchasesandsells.pojo.Inventory;
import com.example.hospital.dispensarypurchasesandsells.pojo.RecordInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InventoryRepository extends JpaRepository<Inventory,Integer> {

    @Query(value = "select new com.example.hospital.dispensarypurchasesandsells.pojo.RecordInfo(m,i) from Medicine m,Inventory i where i.medicineId = m.medicineId")
     List<RecordInfo> findRecordInfo();

    

}
