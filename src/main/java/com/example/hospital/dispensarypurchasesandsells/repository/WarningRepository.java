package com.example.hospital.dispensarypurchasesandsells.repository;

import com.example.hospital.dispensarypurchasesandsells.pojo.RecordInfo;
import com.example.hospital.dispensarypurchasesandsells.pojo.Warning;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface WarningRepository extends JpaRepository<Warning,Integer> {


    @Query(value = "select new com.example.hospital.dispensarypurchasesandsells.pojo.RecordInfo(m,w) from Medicine m,Warning w where w.medicineId = m.medicineId")
    List<RecordInfo> findRecordInfo();
}
