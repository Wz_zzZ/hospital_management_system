package com.example.hospital.dispensarypurchasesandsells.repository;

import com.example.hospital.dispensarypurchasesandsells.pojo.OutputRecord;
import com.example.hospital.dispensarypurchasesandsells.pojo.RecordInfo;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OutputRepository extends JpaRepository<OutputRecord,Integer> {

    @Query(value = "select new com.example.hospital.dispensarypurchasesandsells.pojo.RecordInfo(o,m) from Medicine m,OutputRecord o where o.medicineId = m.medicineId")
    List<RecordInfo> findRecordInfo();

    @Query(value ="select max(o.outputId) from OutputRecord o " )
    int findOutputId();
}
