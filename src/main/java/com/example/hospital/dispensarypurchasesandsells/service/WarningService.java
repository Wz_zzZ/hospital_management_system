package com.example.hospital.dispensarypurchasesandsells.service;

import com.example.hospital.dispensarypurchasesandsells.pojo.RecordInfo;
import com.example.hospital.dispensarypurchasesandsells.pojo.Warning;

import java.util.List;

public interface WarningService {

    List<RecordInfo> findRecordInfoList();


    void save(Warning warning);

    void delete(int id);

    Warning getOne(int id);
}
