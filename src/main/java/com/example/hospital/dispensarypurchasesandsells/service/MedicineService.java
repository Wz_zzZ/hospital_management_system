package com.example.hospital.dispensarypurchasesandsells.service;

import com.example.hospital.dispensarypurchasesandsells.pojo.Medicine;

import java.util.List;

public interface MedicineService {

    int findIdByName(String name);

    int findMaxId();

    void save(Medicine medicine);

    void delete(int id);

    List<String> findMedicineNameByKeyWord(String kw);
}
