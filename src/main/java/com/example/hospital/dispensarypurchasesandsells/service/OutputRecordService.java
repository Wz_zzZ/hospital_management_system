package com.example.hospital.dispensarypurchasesandsells.service;

import com.example.hospital.dispensarypurchasesandsells.pojo.OutputRecord;
import com.example.hospital.dispensarypurchasesandsells.pojo.RecordInfo;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface OutputRecordService {

    List<RecordInfo> findRecordInfoList();

    void save(OutputRecord outputRecord);

    int findMaxOutputId();

    void delete(int id);
}
