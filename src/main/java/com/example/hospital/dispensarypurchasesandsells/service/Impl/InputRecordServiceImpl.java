package com.example.hospital.dispensarypurchasesandsells.service.Impl;

import com.example.hospital.dispensarypurchasesandsells.pojo.InputRecord;
import com.example.hospital.dispensarypurchasesandsells.pojo.RecordInfo;
import com.example.hospital.dispensarypurchasesandsells.repository.InputRepository;
import com.example.hospital.dispensarypurchasesandsells.service.InputRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InputRecordServiceImpl implements InputRecordService {

    @Autowired
    private InputRepository inputRepository;

    @Override
    public List<RecordInfo> findRecordInfoList() {

        List<RecordInfo> recordInfo = inputRepository.findRecordInfo();
        return recordInfo;
    }

    @Override
    public void save(InputRecord inputRecord) {
        inputRepository.save(inputRecord);
    }

    @Override
    public int findMaxInputId() {
        int inputId = inputRepository.findInputId();

        return inputId;
    }

    @Override
    public void delete(int id) {
        inputRepository.deleteById(id);
    }
}
