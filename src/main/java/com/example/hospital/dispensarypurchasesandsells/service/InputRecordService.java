package com.example.hospital.dispensarypurchasesandsells.service;

import com.example.hospital.dispensarypurchasesandsells.pojo.InputRecord;
import com.example.hospital.dispensarypurchasesandsells.pojo.RecordInfo;

import java.util.List;

public interface InputRecordService {

    List<RecordInfo> findRecordInfoList();

    void save(InputRecord inputRecord);

    int findMaxInputId();

    void delete(int id);
}
