package com.example.hospital.dispensarypurchasesandsells.service.Impl;

import com.example.hospital.dispensarypurchasesandsells.pojo.Inventory;
import com.example.hospital.dispensarypurchasesandsells.pojo.RecordInfo;
import com.example.hospital.dispensarypurchasesandsells.repository.InventoryRepository;
import com.example.hospital.dispensarypurchasesandsells.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InventoryServiceImpl implements InventoryService {

    @Autowired
    private InventoryRepository inventoryRepository;

    @Override
    public List<RecordInfo> findRecordInfoList() {
        List<RecordInfo> recordInfo = inventoryRepository.findRecordInfo();

        return recordInfo;
    }

    @Override
    public void save(Inventory inventory) {
        inventoryRepository.save(inventory);
    }

    @Override
    public Inventory findInventoryByMedicineId(int id) {
        Inventory one = inventoryRepository.getOne(id);
        return one;
    }

    @Override
    public void delete(int id) {
        inventoryRepository.deleteById(id);
    }


}
