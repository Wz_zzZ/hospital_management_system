package com.example.hospital.dispensarypurchasesandsells.service.Impl;

import com.example.hospital.dispensarypurchasesandsells.pojo.Medicine;
import com.example.hospital.dispensarypurchasesandsells.repository.MedicineRepo;
import com.example.hospital.dispensarypurchasesandsells.service.MedicineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MedicineServiceImpl implements MedicineService {

    @Autowired
    private MedicineRepo medicineRepository;

    @Override
    public int findIdByName(String name) {
        if (medicineRepository.findByName(name)==null)
            return 0;
        else {
            Medicine medicine = medicineRepository.findByName(name);
            return medicine.getMedicineId();
        }
    }

    @Override
    public int findMaxId() {
        int medicineId = medicineRepository.findMedicineId();

        return medicineId;
    }

    @Override
    public void save(Medicine medicine) {
        medicineRepository.save(medicine);
    }

    @Override
    public void delete(int id) {
        medicineRepository.deleteById(id);
    }

    @Override
    public List<String> findMedicineNameByKeyWord(String kw) {
        List<String> medicineName= medicineRepository.findKeyWord(kw);
        return medicineName;
    }


}
