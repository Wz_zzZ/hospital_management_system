package com.example.hospital.dispensarypurchasesandsells.service;

import com.example.hospital.dispensarypurchasesandsells.pojo.Inventory;
import com.example.hospital.dispensarypurchasesandsells.pojo.RecordInfo;

import java.util.List;

public interface InventoryService {

    List<RecordInfo> findRecordInfoList();

    void save(Inventory inventory);

    Inventory findInventoryByMedicineId(int id);

    void delete(int id);
}
