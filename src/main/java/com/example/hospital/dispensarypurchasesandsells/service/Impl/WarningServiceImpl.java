package com.example.hospital.dispensarypurchasesandsells.service.Impl;

import com.example.hospital.dispensarypurchasesandsells.pojo.Inventory;
import com.example.hospital.dispensarypurchasesandsells.pojo.RecordInfo;
import com.example.hospital.dispensarypurchasesandsells.pojo.Warning;
import com.example.hospital.dispensarypurchasesandsells.repository.InventoryRepository;
import com.example.hospital.dispensarypurchasesandsells.repository.WarningRepository;
import com.example.hospital.dispensarypurchasesandsells.service.WarningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WarningServiceImpl implements WarningService {

    @Autowired
    private WarningRepository warningRepository;
    @Autowired
    private InventoryRepository inventoryRepository;

    @Override
    public List<RecordInfo> findRecordInfoList() {
        List<RecordInfo> recordInfo = warningRepository.findRecordInfo();
        return recordInfo;
    }


    @Override
    public void save(Warning warning) {
        warningRepository.save(warning);
    }

    @Override
    public void delete(int id) {
        warningRepository.deleteById(id);
    }

    @Override
    public Warning getOne(int id) {
        Warning one = warningRepository.getOne(id);

        return one;
    }
}
