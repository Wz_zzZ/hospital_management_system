package com.example.hospital.dispensarypurchasesandsells.service.Impl;

import com.example.hospital.dispensarypurchasesandsells.pojo.OutputRecord;
import com.example.hospital.dispensarypurchasesandsells.pojo.RecordInfo;
import com.example.hospital.dispensarypurchasesandsells.repository.OutputRepository;
import com.example.hospital.dispensarypurchasesandsells.service.OutputRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OutputRecordServiceImpl implements OutputRecordService {

    @Autowired
   private OutputRepository outputRepository;

    @Override
    public List<RecordInfo> findRecordInfoList() {
        List<RecordInfo> recordInfo = outputRepository.findRecordInfo();
        return recordInfo;
    }

    @Override
    public void save(OutputRecord outputRecord) {
        outputRepository.save(outputRecord);
    }

    @Override
    public int findMaxOutputId() {
        int outputId = outputRepository.findOutputId();
        return outputId;
    }

    @Override
    public void delete(int id) {
        outputRepository.deleteById(id);
    }

}
