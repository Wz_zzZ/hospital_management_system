package com.example.hospital.dispensarypurchasesandsells.pojo;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class Medicine {

    @Id
    private int medicineId; //药品id

    private String name;    //药品名称

    private String units;   //药品单位 如：盒，支，片

    private String specification; //药品规格

    @Column(name="purchase_price")
    private float purchasePrice;    //药品进价

    @Column(name="sale_price")
    private float salePrice;    //药品售价

    public int getMedicineId() {
        return medicineId;
    }

    public void setMedicineId(int medicineId) {
        this.medicineId = medicineId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public float getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(float purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public float getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(float salePrice) {
        this.salePrice = salePrice;
    }

    @Override
    public String toString() {
        return "Medicine{" +
                "medicineId=" + medicineId +
                ", name='" + name + '\'' +
                ", units='" + units + '\'' +
                ", specification='" + specification + '\'' +
                ", purchasePrice=" + purchasePrice +
                ", salePrice=" + salePrice +
                '}';
    }
}
