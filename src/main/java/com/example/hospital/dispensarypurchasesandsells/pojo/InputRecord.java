package com.example.hospital.dispensarypurchasesandsells.pojo;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
//@Table(name="inputrecord")
public class InputRecord {
    @Id
    private int inputId;   //进货记录id

    @Column
    private int medicineId; //药品id

    @Column
    private int input;   //进库数量

    @Column
    private Date date; //进库时间

    private String remarks;

    public int getInputId() {
        return inputId;
    }

    public void setInputId(int inputId) {
        this.inputId = inputId;
    }

    public int getMedicineId() {
        return medicineId;
    }

    public void setMedicineId(int medicineId) {
        this.medicineId = medicineId;
    }

    public int getInput() {
        return input;
    }

    public void setInput(int input) {
        this.input = input;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public String toString() {
        return "InputRecord{" +
                "inputId=" + inputId +
                ", medicineId=" + medicineId +
                ", input=" + input +
                ", date=" + date +
                '}';
    }
}