package com.example.hospital.dispensarypurchasesandsells.pojo;


public class RecordInfo {

    private InputRecord inputRecord;
    private Inventory inventory;
    private Medicine medicine;
    private OutputRecord outputRecord;
    private Warning warning;

    public RecordInfo(Medicine medicine,Inventory inventory) {
        this.inventory = inventory;
        this.medicine = medicine;
    }

    public RecordInfo(Medicine medicine,InputRecord inputRecord) {
        this.inputRecord = inputRecord;
        this.medicine = medicine;
    }

    public RecordInfo(OutputRecord outputRecord,Medicine medicine) {
        this.medicine = medicine;
        this.outputRecord = outputRecord;
    }

    public RecordInfo(Medicine medicine,Warning warning) {
        this.medicine = medicine;
        this.warning = warning;
    }

    public RecordInfo() {
    }

    public InputRecord getInputRecord() {
        return inputRecord;
    }

    public void setInputRecord(InputRecord inputRecord) {
        this.inputRecord = inputRecord;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public Medicine getMedicine() {
        return medicine;
    }

    public void setMedicine(Medicine medicine) {
        this.medicine = medicine;
    }

    public OutputRecord getOutputRecord() {
        return outputRecord;
    }

    public void setOutputRecord(OutputRecord outputRecord) {
        this.outputRecord = outputRecord;
    }

    public Warning getWarning() {
        return warning;
    }

    public void setWarning(Warning warning) {
        this.warning = warning;
    }

    @Override
    public String toString() {
        return "RecordInfo{" +
                "inputRecord=" + inputRecord +
                ", inventory=" + inventory +
                ", medicine=" + medicine +
                ", outputRecord=" + outputRecord +
                ", warning=" + warning +
                '}';
    }
}
