package com.example.hospital.dispensarypurchasesandsells.pojo;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Warning {

    @Id
    private int medicineId;

    private int number;

    private boolean status;

    public int getMedicineId() {
        return medicineId;
    }

    public void setMedicineId(int medicineId) {
        this.medicineId = medicineId;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "warning{" +
                "medicineId=" + medicineId +
                ", number=" + number +
                ", status=" + status +
                '}';
    }
}
