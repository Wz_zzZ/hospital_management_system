package com.example.hospital.dispensarypurchasesandsells.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class OutputRecord {

    @Id
    private int outputId;   //出货记录id

    @Column
    private int medicineId; //药品id

    @Column
    private int output;   //出库数量

    @Column
    private Date date; //出库时间

    private String remarks;

    public int getOutputId() {
        return outputId;
    }

    public void setOutputId(int outputId) {
        this.outputId = outputId;
    }

    public int getMedicineId() {
        return medicineId;
    }

    public void setMedicineId(int medicineId) {
        this.medicineId = medicineId;
    }

    public int getOutput() {
        return output;
    }

    public void setOutput(int output) {
        this.output = output;
    }


    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public String toString() {
        return "OutputRecord{" +
                "outputId=" + outputId +
                ", medicineId=" + medicineId +
                ", output=" + output +
                ", date=" + date +
                '}';
    }
}