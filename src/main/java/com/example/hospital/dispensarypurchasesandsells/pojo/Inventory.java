package com.example.hospital.dispensarypurchasesandsells.pojo;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Inventory {    //库存

    @Id
    private int medicineId; //药品id

    public int inventory; //库存数量

    public int getMedicineId() {
        return medicineId;
    }

    public void setMedicineId(int medicineId) {
        this.medicineId = medicineId;
    }

    public int getInventory() {
        return inventory;
    }

    public void setInventory(int inventory) {
        this.inventory = inventory;
    }

    @Override
    public String toString() {
        return "Inventory{" +
                ", medicineId=" + medicineId +
                ", inventory=" + inventory +
                '}';
    }
}
