package com.example.hospital.dispensarypurchasesandsells.controller;

import com.example.hospital.dispensarypurchasesandsells.service.Impl.MedicineServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class MedicineController {
    @Autowired
    private MedicineServiceImpl medicineService;

    @ResponseBody
    @GetMapping("/ajaxSearch")
    public String ajaxSearch(HttpServletRequest request){
        String kw = request.getParameter("kw");
        List<String> name = medicineService.findMedicineNameByKeyWord(kw);

        if(name!=null&&name.size()>0) {
            String str = name.toString();
            str = str.substring(1, str.length() - 1);
            return str;
        }

        return null;
    }
}
