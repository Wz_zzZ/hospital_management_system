package com.example.hospital.dispensarypurchasesandsells.controller;

import com.example.hospital.dispensarypurchasesandsells.pojo.Inventory;
import com.example.hospital.dispensarypurchasesandsells.pojo.RecordInfo;
import com.example.hospital.dispensarypurchasesandsells.pojo.Warning;
import com.example.hospital.dispensarypurchasesandsells.service.Impl.InputRecordServiceImpl;
import com.example.hospital.dispensarypurchasesandsells.service.Impl.InventoryServiceImpl;
import com.example.hospital.dispensarypurchasesandsells.service.Impl.MedicineServiceImpl;
import com.example.hospital.dispensarypurchasesandsells.service.Impl.WarningServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
public class InputRecordController {

    @Autowired
    private InputRecordServiceImpl inputRecordService;

    @Autowired
    private MedicineServiceImpl medicineService;

    @Autowired
    private InventoryServiceImpl inventoryService;

    @Autowired
    private WarningServiceImpl warningService;

    @GetMapping("/inbound")
    public String InputRecord(Model model){

        List<RecordInfo> infoes = inputRecordService.findRecordInfoList();
        model.addAttribute("infoes",infoes);

        return "inbound";
    }

    @PostMapping("addInputRecord")
    public String addMedicine(RecordInfo recordInfo,Model model){

        int id = medicineService.findIdByName(recordInfo.getMedicine().getName());

        if (id==0) {
            model.addAttribute("msg", "此药品不存在");
            List<RecordInfo> infoes = inputRecordService.findRecordInfoList();
            model.addAttribute("infoes",infoes);
            return  "inbound";
        }else {

            if (recordInfo.getInputRecord().getInput()<=0){
                model.addAttribute("msg", "数量不能为负");
                List<RecordInfo> infoes = inputRecordService.findRecordInfoList();
                model.addAttribute("infoes",infoes);
                return  "inbound";
            }else {
                recordInfo.getInputRecord().setMedicineId(id);

                recordInfo.getInputRecord().setDate(new Date());

                recordInfo.getInputRecord().setInputId(inputRecordService.findMaxInputId()+1);

                Inventory inventory = inventoryService.findInventoryByMedicineId(id);

                int inventory1=inventory.getInventory()+recordInfo.getInputRecord().getInput();

                inventory.setInventory(inventory1);

                Warning warning = warningService.getOne(id);

                if (inventory1>warning.getNumber())
                    warning.setStatus(true);
                else
                    warning.setStatus(false);

                warningService.save(warning);

                inventoryService.save(inventory);

                inputRecordService.save(recordInfo.getInputRecord());


                return "redirect:/inbound";
            }
        }
    }

    @PostMapping("/deleteInputRecord")
    public String deleteInputRecord(HttpServletRequest request){

        int inputId = Integer.parseInt(request.getParameter("input_id"));

        inputRecordService.delete(inputId);


        return "redirect:/inbound";
    }

}
