package com.example.hospital.dispensarypurchasesandsells.controller;

import com.example.hospital.dispensarypurchasesandsells.pojo.RecordInfo;
import com.example.hospital.dispensarypurchasesandsells.pojo.Warning;
import com.example.hospital.dispensarypurchasesandsells.service.Impl.InventoryServiceImpl;
import com.example.hospital.dispensarypurchasesandsells.service.Impl.MedicineServiceImpl;
import com.example.hospital.dispensarypurchasesandsells.service.Impl.WarningServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class WarningController {
    @Autowired
    private MedicineServiceImpl medicineService;
    @Autowired
    private WarningServiceImpl warningService;
    @Autowired
    private InventoryServiceImpl inventoryService;

    @GetMapping("/warning")
    public  String warning(Model model){
        List<RecordInfo> infoes=warningService.findRecordInfoList();
        model.addAttribute("infoes",infoes);
        return "warning";
    }

    @PostMapping("addWarning")
    public  String addWarning(RecordInfo recordInfo,Model model){

        int id = medicineService.findIdByName(recordInfo.getMedicine().getName());
        if (id==0) {
            model.addAttribute("msg", "此药品不存在");
            List<RecordInfo> infoes = warningService.findRecordInfoList();
            model.addAttribute("infoes",infoes);
            return  "warning";
        }else {

            if (recordInfo.getWarning().getNumber() <= 0) {
                model.addAttribute("msg", "数量不能为负");
                List<RecordInfo> infoes = warningService.findRecordInfoList();
                model.addAttribute("infoes", infoes);
                return "warning";
            } else {
                recordInfo.getWarning().setMedicineId(id);

                int inventory = inventoryService.findInventoryByMedicineId(recordInfo.getWarning().getMedicineId()).getInventory();
                if (inventory > recordInfo.getWarning().getNumber())
                    recordInfo.getWarning().setStatus(true);
                else
                    recordInfo.getWarning().setStatus(false);


                warningService.save(recordInfo.getWarning());

                return "redirect:/warning";
            }
        }
    }

    @PostMapping("/deleteWarning")
    public String deleteWarning(HttpServletRequest request){

        int medicineId = Integer.parseInt(request.getParameter("medicine_id"));

        warningService.delete(medicineId);


        return "redirect:/warning";
    }

    @PostMapping("/editWarning")
    public String  editWarning(HttpServletRequest request){
        int medicineId = Integer.parseInt(request.getParameter("medicine_id"));
        int number = Integer.parseInt(request.getParameter("number"));

        Warning warning=new Warning();
        warning.setMedicineId(medicineId);
        warning.setNumber(number);

        int inventory = inventoryService.findInventoryByMedicineId(medicineId).getInventory();
        if (inventory>number)
            warning.setStatus(true);
        else
            warning.setStatus(false);

        warningService.save(warning);

        return "redirect:/warning";
    }
}
