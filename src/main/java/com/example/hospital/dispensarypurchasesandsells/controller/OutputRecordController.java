package com.example.hospital.dispensarypurchasesandsells.controller;

import com.example.hospital.dispensarypurchasesandsells.pojo.Inventory;
import com.example.hospital.dispensarypurchasesandsells.pojo.RecordInfo;
import com.example.hospital.dispensarypurchasesandsells.pojo.Warning;
import com.example.hospital.dispensarypurchasesandsells.repository.OutputRepository;
import com.example.hospital.dispensarypurchasesandsells.service.Impl.InventoryServiceImpl;
import com.example.hospital.dispensarypurchasesandsells.service.Impl.MedicineServiceImpl;
import com.example.hospital.dispensarypurchasesandsells.service.Impl.OutputRecordServiceImpl;
import com.example.hospital.dispensarypurchasesandsells.service.Impl.WarningServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@Controller
public class OutputRecordController {

    @Autowired
    private OutputRecordServiceImpl outputRecordService;

    @Autowired
    private MedicineServiceImpl medicineService;

    @Autowired
    private InventoryServiceImpl inventoryService;

    @Autowired
    private WarningServiceImpl warningService;

    @GetMapping("/outbound")
    public String outputRecord(Model model){

        List<RecordInfo> infoes = outputRecordService.findRecordInfoList();
        model.addAttribute("infoes",infoes);

        return "outbound";
    }

    @PostMapping("addOutputRecord")
    public String addOutputRecord(RecordInfo recordInfo,Model model){

        int id = medicineService.findIdByName(recordInfo.getMedicine().getName());
        if (id==0) {
            model.addAttribute("msg", "此药品不存在");
            List<RecordInfo> infoes = outputRecordService.findRecordInfoList();
            model.addAttribute("infoes", infoes);
            return "outbound";
        }else {
            if (recordInfo.getOutputRecord().getOutput()<=0){
                model.addAttribute("msg", "数量不能为负");
                List<RecordInfo> infoes = outputRecordService.findRecordInfoList();
                model.addAttribute("infoes",infoes);
                return  "outbound";
            }else {

                recordInfo.getOutputRecord().setMedicineId(id);

                recordInfo.getOutputRecord().setDate(new Date());

                recordInfo.getOutputRecord().setOutputId(outputRecordService.findMaxOutputId() + 1);

                Inventory inventory = inventoryService.findInventoryByMedicineId(id);

                int inventory1 = inventory.getInventory() - recordInfo.getOutputRecord().getOutput();

                inventory.setInventory(inventory1);

                Warning warning = warningService.getOne(id);

                if (inventory1 > warning.getNumber())
                    warning.setStatus(true);
                else
                    warning.setStatus(false);

                warningService.save(warning);

                inventoryService.save(inventory);

                outputRecordService.save(recordInfo.getOutputRecord());


                return "redirect:/outbound";
            }
        }
    }

    @PostMapping("/deleteOutputRecord")
    public String deleteOutputRecord(HttpServletRequest request){


        int outputId = Integer.parseInt(request.getParameter("output_id"));

        outputRecordService.delete(outputId);


        return "redirect:/outbound";
    }

}
