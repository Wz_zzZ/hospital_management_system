package com.example.hospital.patientinformation.repository;

import com.example.hospital.patientinformation.pojo.Patienti;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PatientinRepository extends JpaRepository<Patienti,Integer>{

    //查找方式
    public Patienti findById(String id);

    //删除方式
    public void deleteById(String id);


}
