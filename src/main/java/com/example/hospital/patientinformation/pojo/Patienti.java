package com.example.hospital.patientinformation.pojo;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity()
@Table(name="patientiinfo")
@Data
public class Patienti {
    @Id                   //定义患者信息实体类
    private String id;      //主键

    private String name;
    private int age;
    private String sex;
    private String born;
    private String pphone;
    private String ophone;
    private String ethnic;
    private String education;
    private String marry;
    private String blood;
    private String casehistory;
    private String orderdata;


}
