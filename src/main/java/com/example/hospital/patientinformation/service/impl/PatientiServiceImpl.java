package com.example.hospital.patientinformation.service.impl;

import com.example.hospital.patientinformation.pojo.Patienti;
import com.example.hospital.patientinformation.repository.PatientinRepository;
import com.example.hospital.patientinformation.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class PatientiServiceImpl implements PatientService {
    @Autowired
    private PatientinRepository patientinRepository;

    //患者信息保存的实现
    @Override
    public void Add(String id, String name, String sex,int age, String born, String pphone, String ophone, String ethnic, String education, String marry,String blood, String casehistory, String orderdata) {
        Patienti patienti=new Patienti();

        patienti.setId(id);
        patienti.setName(name);
        patienti.setBorn(born);
        patienti.setSex(sex);
        patienti.setAge(age);
        patienti.setPphone(pphone);
        patienti.setOphone(ophone);
        patienti.setEthnic(ethnic);
        patienti.setEducation(education);
        patienti.setMarry(marry);
        patienti.setBlood(blood);
        patienti.setCasehistory(casehistory);
        patienti.setOrderdata(orderdata);

        patientinRepository.save(patienti);
    }

    //通过id删除患者信息\
    @Transactional
    @Override
    public void Delete(String id) {

        patientinRepository.deleteById(id);
      /* String name=patientinRepository.findById(id).getName();
       System.out.println(name);*/
    }

    //患者信息保存的实现
    @Transactional
    @Override
    public void Update(String id, String name, String sex,int age, String born, String pphone, String ophone, String ethnic, String education, String marry,String blood, String casehistory, String orderdata) {
        Patienti patienti=patientinRepository.findById(id);
        patienti.setName(name);
        patienti.setBorn(born);
        patienti.setSex(sex);
        patienti.setAge(age);
        patienti.setPphone(pphone);
        patienti.setOphone(ophone);
        patienti.setEthnic(ethnic);
        patienti.setEducation(education);
        patienti.setMarry(marry);
        patienti.setBlood(blood);
        patienti.setCasehistory(casehistory);
        patienti.setOrderdata(orderdata);

        patientinRepository.save(patienti);
    }

    //通过id进行查找患者信息
    @Override
    public Patienti checkPatienti(String id) {

        return patientinRepository.findById(id);

    }

    //查询所有患者信息
    public List<Patienti> checkPatientis(){

        return patientinRepository.findAll();
        }


    //查询今日需要医治的患者
    @Override
    public List<Patienti> checkWorkPatientis() {
        Date d = new Date();
        System.out.println(d);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dateNowStr = sdf.format(d);
        //System.out.println("格式化后的日期：" + dateNowStr);

        List<Patienti> all = patientinRepository.findAll();
        List<Patienti> work=new ArrayList<>();
        for (Patienti patienti:all ) {
            if(patienti.getOrderdata().equals(dateNowStr)) {
               // System.out.println(patienti.getOrderdata());
                work.add(patienti);
            }
        }
        return work;
    }

}