package com.example.hospital.patientinformation.service;

import com.example.hospital.patientinformation.pojo.Patienti;
import com.example.hospital.patientinformation.repository.PatientinRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

public interface PatientService {

    void Add(String id, String name, String sex, int age, String born, String pphone, String ophone, String ethnic, String education, String marry, String blood, String casehistory, String orderdata);

    void Delete(String id);

    void Update(String id, String name, String sex, int age, String born, String pphone, String ophone, String ethnic, String education, String marry, String blood, String casehistory, String orderdata);

    Patienti checkPatienti(String id);

    List<Patienti> checkPatientis();

    List<Patienti> checkWorkPatientis();
}
