package com.example.hospital.patientinformation.controller;

import com.example.hospital.patientinformation.pojo.Patienti;
import com.example.hospital.patientinformation.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import sun.security.krb5.internal.PAData;

import java.util.List;

@Controller
public class uopdataCotroller {
    @Autowired
    private PatientService patientService;

    //主页面的载入
    @RequestMapping("/titlel")
    public String title(){

        return "./lixue/top/title";
    }


    @RequestMapping("/workl")
    public String work(Model model){
        List<Patienti> patientis = patientService.checkWorkPatientis();
        model.addAttribute("patientis",patientis);
        return "./lixue/right/work";
    }


    @RequestMapping("/herfl")
    public String herf(){

        return "./lixue/left/herf";
    }

    //跳转到add.html
    @RequestMapping("/add01l")
    public String add(){

        return "./lixue/right/add";
    }

    //跳转到patientiInfoOut.html
    @RequestMapping("/patientiInfoOutl")
    public String patientiInfoOut(){

        return "./lixue/right/patientiInfoOut";
    }

    //跳转到患者名单列表
    @RequestMapping("/infoListl")
    public String infoList(Model model){

        List<Patienti> patientis = patientService.checkPatientis();
        model.addAttribute("patientis",patientis);
        return "./lixue/right/infoList";
    }

    //跳转到rewrite.html
    @RequestMapping("/rewritel")
    public String rewrite(String id,Model model){
        Patienti patienti=patientService.checkPatienti(id);
        model.addAttribute("patienti",patienti);
        return "./lixue/right/rewrite";
    }
}
