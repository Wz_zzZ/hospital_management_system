package com.example.hospital.patientinformation.controller;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.JSONPObject;
import com.example.hospital.patientinformation.pojo.Patienti;
import com.example.hospital.patientinformation.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@Controller
public class patientiInfoControler {

    @Autowired
    private PatientService patientService;


    //信息的添加
    @ResponseBody
    @RequestMapping("/addl")
    public String addInfo(String id, String name, String sex,Integer age, String born,String pphone,String ophone,String ethnic,String education,String marry,String blood,String casehistory,String orderdata){
        System.out.println(id);
        patientService.Add(id,name,sex,age,born,pphone,ophone,ethnic,education,marry,blood,casehistory,orderdata);
      /*  JSONObject jsonObjectResult=new JSONObject();
        jsonObjectResult.put("msg", id);
        return jsonObjectResult;*/
      return "保存成功";
    }


    //患者信息的获取
    @RequestMapping("/checkl")
    public String checkInfo(String id, Model model){

        Patienti patienti = patientService.checkPatienti(id);
        model.addAttribute("pa",patienti);

        return "./lixue/right/patientiInfoOut";
    }

    //患者信息的删除
    @RequestMapping("/deletel")
    public String deleteinfo(String id,Model model){

        patientService.Delete(id);

        List<Patienti> patientis = patientService.checkPatientis();
        model.addAttribute("patientis",patientis);

        //System.out.println("go!--------------------------------------------");
        return "./lixue/right/work";
    }

}
