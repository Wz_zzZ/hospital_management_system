package com.example.hospital.appointment.pojo;


import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
/*
@auther 郭起
@create 2019/05/31

 */

@Entity(name="appointinf")

public class AppointInf {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String sex;
    private String pphone;
    private String ophone;
    private int age;
    private String nation;
    private String appointdata;
    private String appointneed;
    private String bloodshape;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }



    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getAppointdata() {
        return appointdata;
    }

    public void setAppointdata(String appointdata) {
        this.appointdata = appointdata;
    }
    public String getPphone() {
        return pphone;
    }

    public void setPphone(String pphone) {
        this.pphone = pphone;
    }

    public String getOphone() {
        return ophone;
    }

    public void setOphone(String ophone) {
        this.ophone = ophone;
    }
    public String getAppointneed() {
        return appointneed;
    }

    public void setAppointneed(String appointneed) {
        this.appointneed = appointneed;
    }

    public String getBloodshape() {
        return bloodshape;
    }

    public void setBloodshape(String bloodshape) {
        this.bloodshape = bloodshape;
    }

    @Override
    public String toString() {
        return "AppointInf{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", pphone='" + pphone + '\'' +
                ", ophone='" + ophone + '\'' +
                ", appointneed='" + appointneed + '\'' +
                ", bloodshape='" + bloodshape + '\'' +
                ", age='" + age + '\'' +
                ", nation='" + nation+ '\'' +
                ", appointdata='" + appointdata + '\'' +
                '}';
    }
}
