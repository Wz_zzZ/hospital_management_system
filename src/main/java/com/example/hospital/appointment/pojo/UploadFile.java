package com.example.hospital.appointment.pojo;

import javax.persistence.Entity;
import javax.persistence.Id;


@Entity(name = "uploadfile")
public class UploadFile {

    @Id
    private int id;

    private String name;

    private String type;

    private String path;

    public UploadFile() {
        this.id = id;
        this.name = name;
        this.type=type;
        this.path = path;
    }



    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "FileInfo{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ",path='" + path + '\'' +
                '}';
    }
}
