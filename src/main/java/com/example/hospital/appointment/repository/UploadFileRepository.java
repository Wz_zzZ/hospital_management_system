package com.example.hospital.appointment.repository;



import com.example.hospital.appointment.pojo.UploadFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 */
@Repository
public interface UploadFileRepository extends JpaRepository<UploadFile, Integer> {
    //定义一个repository，首先要继承一下 JpaRepository 它有两个范型 <T, ID> ,T是实体类，主键的类型

    UploadFile findAllById(Integer id);

}
