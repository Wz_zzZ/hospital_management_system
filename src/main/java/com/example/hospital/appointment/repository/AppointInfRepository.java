package com.example.hospital.appointment.repository;

import com.example.hospital.appointment.pojo.AppointInf;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface AppointInfRepository extends JpaRepository<AppointInf,Integer> {

    //查找方式
    public AppointInf findById(int id);

    //删除方式
    public void deleteById(String id);

}
