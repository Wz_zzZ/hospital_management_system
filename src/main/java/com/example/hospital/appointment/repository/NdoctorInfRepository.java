package com.example.hospital.appointment.repository;

import com.example.hospital.appointment.pojo.NdoctorInf;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;



@Repository
public interface NdoctorInfRepository extends JpaRepository<NdoctorInf,Integer>, JpaSpecificationExecutor<NdoctorInf> {

    //查找方式
    public NdoctorInf findById(int id);
    NdoctorInf findByName(String name);
    /* 删除方式 */
    public void deleteById(int id);


 }

