package com.example.hospital.appointment.service;

import com.example.hospital.appointment.pojo.NdoctorInf;
import org.springframework.data.domain.Page;

import java.util.List;

public interface NdoctorInfService {


    void Add(int id,String name,String  sex,String  direction,String frofile, String speciality );

    void Delete(int id);

    void Update(int id, String name, String sex, String direction,String frofile, String speciality);

    public NdoctorInf selectById(int id);


    void save(NdoctorInf ndoctorInf);

    public Page<NdoctorInf> listPager(NdoctorInf ndoctorInf, Integer pageSize, Integer page);

    void Update(NdoctorInf ndoctorInf);

    List<NdoctorInf> findAll();

    NdoctorInf findById(int id);
}
