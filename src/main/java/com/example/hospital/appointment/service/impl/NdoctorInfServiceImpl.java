package com.example.hospital.appointment.service.impl;
import com.example.hospital.appointment.pojo.NdoctorInf;
import com.example.hospital.appointment.repository.NdoctorInfRepository;
import com.example.hospital.appointment.service.NdoctorInfService;
import jdk.nashorn.internal.runtime.Specialization;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Service
public class NdoctorInfServiceImpl implements NdoctorInfService {

    @Autowired
    private NdoctorInfRepository ndoctorInfRepository;


    //添加医生信息
    public void Add(int id, String name, String sex, String direction, String frofile, String speciality) {
        NdoctorInf ndoctorInf=new NdoctorInf();
        ndoctorInf.setId(id);
        ndoctorInf.setName(name);
        ndoctorInf.setSex(sex);
        ndoctorInf.setDirection(direction);
        ndoctorInf.setFrofile(frofile);
        ndoctorInf.setSpeciality(speciality);
         ndoctorInfRepository.save(ndoctorInf);
    }

    @Transactional
    public void Delete (int id)
    {
        ndoctorInfRepository.deleteById(id);
    }
    @Override
    public NdoctorInf selectById(int id){
        return ndoctorInfRepository.findById(id);
    }

    @Override
    public void save(NdoctorInf ndoctorInf) {
        ndoctorInfRepository.save(ndoctorInf);
    }
    @Override
    public void Update(NdoctorInf ndoctorInf){
        ndoctorInfRepository.save(ndoctorInf);
    }

    @Override
    public List<NdoctorInf> findAll() {
        return ndoctorInfRepository.findAll();
    }

    @Override
    public NdoctorInf findById(int id) {
        return ndoctorInfRepository.findById(id);
    }

    //编辑修改医生信息
    @Override
    @Transactional
    public void Update(int id, String name, String sex, String direction,String frofile, String speciality) {
        NdoctorInf ndoctorInf=ndoctorInfRepository.findById(id);
        System.out.println(id);
        if (ndoctorInf==null){
            System.out.println("查询失败");
        }
        ndoctorInf.setId(id);
        ndoctorInf.setName(name);
        ndoctorInf.setSex(sex);
        ndoctorInf.setDirection(direction);
        ndoctorInf.setFrofile(frofile);
        ndoctorInf.setSpeciality(speciality);
        ndoctorInfRepository.save(ndoctorInf);
    }

    @Override
    public Page<NdoctorInf> listPager(NdoctorInf ndoctorInf, Integer pageSize, Integer page){
        Pageable pageable = new PageRequest(page, pageSize, Sort.Direction.ASC, "id");//分页按id递增
        Page<NdoctorInf> ndoctorInfsPage = ndoctorInfRepository.findAll(new Specification<NdoctorInf>() {
            @Override
            public Predicate toPredicate(Root<NdoctorInf> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> list = new ArrayList<>();
                if(!StringUtils.isEmpty(ndoctorInf.getName())){
                    list.add(criteriaBuilder.equal(root.get("name").as(String.class), ndoctorInf.getName()));
                }   //通过name查找
                Predicate [] p = new Predicate[list.size()];
                return criteriaBuilder.and(list.toArray(p));
            }
        }, pageable);
        return ndoctorInfsPage;
    }


}
