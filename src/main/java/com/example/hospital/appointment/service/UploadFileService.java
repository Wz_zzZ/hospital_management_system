package com.example.hospital.appointment.service;

import com.example.hospital.appointment.pojo.UploadFile;

import java.util.List;

public interface UploadFileService {


    /**
     * 根据文件ID查询附件信息
     *
     * @param fileId
     * @return
     */
    UploadFile getUploadFileByFileId(Integer fileId);

    /**
     * 保存文件信息
     *
     * @param uploadFile
     * @return
     */
    UploadFile save(UploadFile uploadFile);

    List<UploadFile> findAll();
}

