package com.example.hospital.appointment.service;

import com.example.hospital.appointment.pojo.AppointInf;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AppointInfService {
    List<AppointInf> findAll();
    void appointAdd(String name,String sex,String pphone,String ophone,int age,String nation,String appointdata,String appointneed,String bloodshape);
    void Delete(int id);

    public AppointInf selectById(int id);

    void save(AppointInf appointInf);

    List<AppointInf> checkWorkAppoint();


}
