package com.example.hospital.appointment.service.impl;

import com.example.hospital.appointment.pojo.AppointInf;
import com.example.hospital.appointment.repository.AppointInfRepository;
import com.example.hospital.appointment.service.AppointInfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class AppointInfServiceImpl implements AppointInfService {
    @Autowired
    private AppointInfRepository appointInfRepository;

    @Override
    public List<AppointInf> findAll() {
        return appointInfRepository.findAll();
    }

    @Override
    public void appointAdd( String name, String sex, String pphone, String ophone, int age, String nation, String appointdata, String appointneed, String bloodshape) {
        AppointInf appointInf=new AppointInf();
        appointInf.setName(name);
        appointInf.setSex(sex);
        appointInf.setAge(age);
        appointInf.setAppointdata(appointdata);
        appointInf.setAppointneed(appointneed);
        appointInf.setBloodshape(bloodshape);
        appointInf.setNation(nation);
        appointInf.setOphone(ophone);
        appointInf.setPphone(pphone);
        appointInfRepository.save(appointInf);
    }

    @Override
    public void Delete(int id) {
       appointInfRepository.deleteById(id);
    }

    /*   @Override
    public Patienti checkPatienti(String id) {
        return patientinRepository.findById(id);
    }*/
    @Override
    public AppointInf selectById(int id) {
        return appointInfRepository.findById(id);
    }

    @Override
    public void save(AppointInf appointInf) {
        appointInfRepository.save(appointInf);
    }

    //查询预约的患者
    @Override
    public List<AppointInf> checkWorkAppoint() {
        Date d = new Date();
        System.out.println(d);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String dateNowStr = sdf.format(d);
        //System.out.println("格式化后的日期：" + dateNowStr);
        List<AppointInf> all = appointInfRepository.findAll();
        List<AppointInf> work=new ArrayList<>();
        for (AppointInf appointInf:all ) {
            try {
                if(d.before(sdf.parse(appointInf.getAppointdata()))||dateNowStr.equals(appointInf.getAppointdata())) {
                    // System.out.println(patienti.getOrderdata());
                    work.add(appointInf);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        System.out.println(work.size());
        return work;
    }

}
