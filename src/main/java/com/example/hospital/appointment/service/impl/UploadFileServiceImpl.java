package com.example.hospital.appointment.service.impl;

import com.example.hospital.appointment.pojo.UploadFile;
import com.example.hospital.appointment.repository.UploadFileRepository;
import com.example.hospital.appointment.service.UploadFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class UploadFileServiceImpl implements UploadFileService {
    @Autowired
    UploadFileRepository uploadFileRepository;


    @Override
    public UploadFile getUploadFileByFileId(Integer fileId) {

        return uploadFileRepository.findAllById(fileId);
    }

    @Override
    public UploadFile save(UploadFile uploadFile) {
        return uploadFileRepository.save(uploadFile);
    }

    @Override
    public List<UploadFile> findAll() {
        return uploadFileRepository.findAll();
    }


}
