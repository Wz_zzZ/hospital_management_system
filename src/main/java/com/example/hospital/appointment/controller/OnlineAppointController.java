package com.example.hospital.appointment.controller;


import com.example.hospital.appointment.pojo.AppointInf;
import com.example.hospital.appointment.service.AppointInfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;

@Controller
public class OnlineAppointController {
    @Autowired
    private AppointInfService appointInfService;

    //跳转到在线挂号的界面
    @RequestMapping("appointment")
    public String appointment(){
        return "appointment";
    }

  /*  @RequestMapping("appointnow")
    public String appointment(AppointInf appointInf){
        appointInfService.save(appointInf);
        return "预约成功";
    }*/
     @RequestMapping("appointnow")
    public  String appointAdd(String name, String sex,String pphone, String ophone, int age, String nation, String appointdata, String appointneed, String bloodshape)
     {
         appointInfService.appointAdd(name, sex,pphone,ophone, age,nation,appointdata,appointneed,bloodshape);

      return "main";
     }


       //遍历预约成功且未过期的患者
    @RequestMapping("appointmentinformationg")
    public String adminappointinfor(Model model){
         List<AppointInf> appointInfs=appointInfService.checkWorkAppoint();
         model.addAttribute("appointInfs",appointInfs);
        return "appointmentinformation";
    }

    //医生查询患者详细信息
    @RequestMapping("appointmentinforoutg")
    public String adminappointinfout(Model model,int id){
       AppointInf appointInf=appointInfService.selectById(id);
       model.addAttribute("check",appointInf);
        return "appointmentinforout";
    }
 /*   //患者信息的删除
    @RequestMapping("/delete")
    public String deleteinfo(String id,Model model){
        patientService.Delete(id);
        List<Patienti> patientis = patientService.checkPatientis();
        model.addAttribute("patientis",patientis);
        //System.out.println("go!--------------------------------------------");
        return "./right/work";
    }*/
    //删除预约的信息
    @RequestMapping("appointdelete")
    public String appoimtdelete(int id,Model model){
         appointInfService.Delete(id);
        List<AppointInf> appointInfs=appointInfService.checkWorkAppoint();
        model.addAttribute("appointInfs",appointInfs);
         return "appointmentinformation";
    }

}
