package com.example.hospital.appointment.controller;


import com.example.hospital.appointment.pojo.NdoctorInf;
import com.example.hospital.appointment.service.NdoctorInfService;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class NdoctorInfControler {

    @Autowired
    private NdoctorInfService ndoctorInfService;

    public static final String ROOT = "D:/IDEA/workspace/doctorinfimage/";

    private final ResourceLoader resourceLoader;
    @Autowired
    public NdoctorInfControler(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    //编辑保存信息
    @RequestMapping("/edit")
    public String editNdoctorInf(Map map, int id, String name, String sex, String direction,String frofile, String speciality) {
        System.out.println(id);
        ndoctorInfService.Update(id,name,sex,direction,frofile, speciality);
        //编辑保存后，跳转到doctorinf页面，并且把跟新后的信息全部上传
        return "redirect:/doctorinfor";
    }

    @RequestMapping("doctorinfor")
    public String updataNdoctorInfor(Map map){
        List<List> allDoctorInfo = new ArrayList();
        List<NdoctorInf> ndoctorInf = ndoctorInfService.findAll();
        List<NdoctorInf> threeDoctorInfo = null;
        int mark1 = 0;
        int mark2 = 0;
        for(int i=1;i<=ndoctorInf.size();i++){
            if(mark1==mark2) {
                threeDoctorInfo = new ArrayList<>();
                mark1++;
            }
            threeDoctorInfo.add(ndoctorInf.get(i-1));
                if (i % 3 == 0){
                    allDoctorInfo.add(threeDoctorInfo);
                    mark2++;
                }
        }
        map.put("doctors",allDoctorInfo);
        return "doctorinfor";
    }



    @GetMapping("img/{dockerId}")
    @ResponseBody
    public ResponseEntity<?> getFile(@PathVariable int dockerId) {
        try {
            NdoctorInf ndoctorInf = ndoctorInfService.selectById(dockerId);
            return ResponseEntity.ok(resourceLoader.getResource("file:" + Paths.get(ROOT, ndoctorInf.getPicture())));
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
    /**
     * @api {get} file/{appId}/upload
     * @apiGroup File
     */
    @PostMapping("{dockerId}/upload")
    public String handleFileUpload(@RequestParam("file") MultipartFile file, @PathVariable int dockerId) {
        if (!file.isEmpty()) {
            try {
                NdoctorInf ndoctorInf = ndoctorInfService.selectById(dockerId);    // 从数据库中查找
                Files.copy(file.getInputStream(), Paths.get(ROOT, file.getOriginalFilename()));
                // D:/IDEA/workspace/doctorinfimage/实验室的记忆.jpg
                ndoctorInf.setPicture(file.getOriginalFilename());  //file.getOriginalFilename() = 实验室的记忆.jpg
                ndoctorInfService.save(ndoctorInf);
                return "redirect:/doctorinfor";
            } catch (IOException|RuntimeException e) {
                e.printStackTrace();
            }
        }
        return "redirect:/doctorinfor";
    }

    private void clearPreviousFile(String filename){
        File file = new File(ROOT+ filename);
        if(file.exists()){
            file.delete();
        }
    }

           //从left跳转到预约界面，同时把数据库的信息传入
           @RequestMapping("onlineappoint")
           public String onlineNdoctorInfor(Map map){
               List<List> allDoctorInfo = new ArrayList();
               List<NdoctorInf> ndoctorInf = ndoctorInfService.findAll();
               List<NdoctorInf> threeDoctorInfo = null;
               int mark1 = 0;
               int mark2 = 0;
               for(int i=1;i<=ndoctorInf.size();i++){
                   if(mark1==mark2) {
                       threeDoctorInfo = new ArrayList<>();
                       mark1++;
                   }
                   threeDoctorInfo.add(ndoctorInf.get(i-1));
                   if (i % 3 == 0){
                       allDoctorInfo.add(threeDoctorInfo);
                       mark2++;
                   }
               }
               map.put("doctors",allDoctorInfo);
               for (List list : allDoctorInfo) {
                   System.out.println(list.size());
               }
               System.out.println(allDoctorInfo.size());
               System.out.println(ndoctorInf.size());
               return "onlineappoint";
           }

            //删除医生的信息
            @RequestMapping("/delete")
            public String deletedoctorinfo(int id,Map map){
                ndoctorInfService.Delete(id);
                List<List> allDoctorInfo = new ArrayList();
                List<NdoctorInf> ndoctorInf = ndoctorInfService.findAll();
                List<NdoctorInf> threeDoctorInfo = null;
                int mark1 = 0;
                int mark2 = 0;
                for(int i=1;i<=ndoctorInf.size();i++){
                    if(mark1==mark2) {
                        threeDoctorInfo = new ArrayList<>();
                        mark1++;
                    }
                    threeDoctorInfo.add(ndoctorInf.get(i-1));
                    if (i % 3 == 0){
                        allDoctorInfo.add(threeDoctorInfo);
                        mark2++;
                    }
                }
                map.put("doctors",allDoctorInfo);
                return "doctorinfor";
            }

             //介绍医生信息
             //跳转到简介的界面
             @RequestMapping("introduction")
                 public String introduction(Model model,int id){
                     NdoctorInf ndoctorInfs = ndoctorInfService.findById(id);
                     model.addAttribute("doctorintroduce",ndoctorInfs);
                 return "introduction";
             }


            //添加医生信息
            @RequestMapping("adddoctor")
            public String adddoctor(){
                return "adddoctorinfor";
            }

            @RequestMapping("add")
                    public String adddoctor(int id, String name, String sex, String direction, String frofile, String speciality){
                        ndoctorInfService.Add(id,name,sex,direction, frofile, speciality);
                        return "main";
                    }



}
