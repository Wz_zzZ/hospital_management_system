package com.example.hospital.physicalExamination.controller;

import com.example.hospital.physicalExamination.entity.PeItems;
import com.example.hospital.physicalExamination.entity.PeUser;
import com.example.hospital.physicalExamination.service.PeItemsService;
import com.example.hospital.physicalExamination.service.PeUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Date;


@Controller
public class PeItemsPageController {

    @Autowired
    PeItemsService peItemsService;

    @Autowired
    PeUserService peUserService;

    @RequestMapping("/pe/peItemsIndex")
    public String index(Model model,Long id){
        model.addAttribute("userId",id);
        return "/pe/peItemsIndex";
    }

    //查看
    @RequestMapping("pe/peUserManage/peItemsList1")
    public String peItemsList1(Model model,Long id){
        model.addAttribute("i",1);
        model.addAttribute("userId",id);
        return "forward:/pe/peUserManage/peItemsList";
    }
    //编辑
    @RequestMapping("pe/peUserManage/peItemsList2")
    public String peItemsList2(Model model,Long id){
        model.addAttribute("i",2);
        model.addAttribute("userId",id);
        return "forward:/pe/peUserManage/peItemsList";
    }
    //删除
    @RequestMapping("pe/peUserManage/peItemsList3")
    public String peItemsList3(Model model,Long id){
        model.addAttribute("i",3);
        model.addAttribute("userId",id);
        return "forward:/pe/peUserManage/peItemsList";
    }

    @RequestMapping(value = "/pe/peUserManage/peItemsList")
    public String peItemsList(Model model ,Long id){
        String userId = id+"";
        Page<PeItems> peItems = peItemsService.findItems(0,10,userId);
        model.addAttribute("peItems",peItems);
        model.addAttribute("userId",userId);
        return "/pe/peUserManage/peItemsList";
    }

    @PostMapping("/pe/peUserManage/peItemsList/{page}")
    @ResponseBody
    public Page<PeItems> peItemsList(Model model,@PathVariable Integer page,@RequestBody PeItems peItems){
        String id = peItems.getUserId();
        System.out.println(peItems.toString());
        return peItemsService.findItems(page,10,id+"");
    }

    @RequestMapping("/pe/peUserManage/toQueryPeItems")
    public String toQueryPeItems(Model model,Long id){
        PeItems peItems = peItemsService.findPeItemsById(id);
        model.addAttribute("peItems",peItems);
        return "/pe/peUserManage/queryPeItems";
    }

    @RequestMapping("/pe/peUserManage/toAddPeItems")
    public String toAddPeItems(Model model,Long id){
        model.addAttribute("userId",id);
        return "/pe/peUserManage/addPeItems";
    }

    @RequestMapping("/pe/peUserManage/addPeItems")
    public String addPeItems(PeItems peItems){
        Date date = new Date();
        peItems.setCreateDate(date);
        peItemsService.save(peItems);
        return "redirect:/pe/peUserManage/peItemsList1?id=" + peItems.getUserId();
    }

    @RequestMapping("/pe/peUserManage/toEditPeItems")
    public String toEditPeItems(Model model,Long id){
        PeItems peItems=peItemsService.findPeItemsById(id);
        model.addAttribute("peItems",peItems);
        return "/pe/peUserManage/editPeItems";
    }

    @RequestMapping("/pe/peUserManage/editPeItems")
    public String editPeItems(PeItems peItems){
        Date date = new Date();
        peItems.setUpdateDate(date);
        PeItems peItems1 = peItemsService.findPeItemsById(peItems.getId());
        peItems.setUserId(peItems1.getUserId());
        peItems.setCreateDate(peItems1.getCreateDate());
        peItemsService.edit(peItems);
        return "redirect:/pe/peUserManage/peItemsList2?id=" + peItems.getUserId();
    }

    @RequestMapping("/pe/peUserManage/toDeletePeItems")
    public String toDeletePeItems(Long id){
        String userId = peItemsService.findPeItemsById(id).getUserId();
        peItemsService.delete(id);
        return "redirect:/pe/peUserManage/peItemsList3?id=" + Integer.parseInt(userId);
    }

    @RequestMapping("/pe/peUserManage/toQueryItemsInfo")
    public String toQueryItemsInfo(Model model){
        PeUser peUser1 = (PeUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        PeUser peUser2 = peUserService.findPeUserByIdNumber(peUser1.getUsername());
        return "redirect:/pe/peUserManage/peItemsList1?id=" + peUser2.getId();
    }
}
