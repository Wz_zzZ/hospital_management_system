package com.example.hospital.physicalExamination.controller;

import com.example.hospital.physicalExamination.entity.PeRole;
import com.example.hospital.physicalExamination.entity.PeUser;
import com.example.hospital.physicalExamination.service.PeItemsService;
import com.example.hospital.physicalExamination.service.PeRoleService;
import com.example.hospital.physicalExamination.service.PeUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;


@Controller
public class PeUserPageController {

    @Autowired
    PeUserService peUserService;

    @Autowired
    PeRoleService peRoleService;

    @RequestMapping("/pe/index")
    public String index(Model model){
        PeUser peUser = (PeUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        PeUser peUser1 = peUserService.findPeUserByIdNumber(peUser.getUsername());
        model.addAttribute("user",peUser1);
        return "/pe/index";
    }

    //查看
    @RequestMapping("pe/peUserManage/peUserList1")
    public String peUserList1(Model model){
        model.addAttribute("i",1);
        return "forward:/pe/peUserManage/peUserList";
    }
    //编辑
    @RequestMapping("pe/peUserManage/peUserList2")
    public String peUserList2(Model model){
        model.addAttribute("i",2);
        return "forward:/pe/peUserManage/peUserList";
    }
    //授权
    @RequestMapping("pe/peUserManage/peUserList3")
    public String peUserList3(Model model){
        model.addAttribute("i",3);
        return "forward:/pe/peUserManage/peUserList";
    }
    //删除
    @RequestMapping("pe/peUserManage/peUserList4")
    public String peUserList4(Model model){
        model.addAttribute("i",4);
        return "forward:/pe/peUserManage/peUserList";
    }
    //查询体检信息列表
    @RequestMapping("pe/peUserManage/peUserList5")
    public String peUserList5(Model model){
        model.addAttribute("i",5);
        return "forward:/pe/peUserManage/peUserList";
    }

    @RequestMapping(value = "/pe/peUserManage/peUserList")
    public String query(Model model, PeUser peUser){
        Page<PeUser> peUsers = peUserService.findPeUsers(0, 3, peUser);
        model.addAttribute("peUsers",peUsers);
        return "/pe/peUserManage/peUserList";
    }

    @PostMapping("/pe/peUserManage/peUserList/{page}")
    @ResponseBody
    public Page<PeUser> query(@PathVariable Integer page, @RequestBody PeUser peUser){
        System.out.println(peUser.toString());
        return peUserService.findPeUsers(page,3,peUser);
    }

    @RequestMapping("/pe/peUserManage/toQueryPeUser")
    public String toQueryPeUser(Model model,Long id){
        PeUser peUser = peUserService.findPeUserById(id);
        model.addAttribute("peUser",peUser);
        return "/pe/peUserManage/queryPeUser";
    }

    @RequestMapping("/pe/peUserManage/toEditPeUser")
    public String toEditPeUser(Model model,Long id){
        PeUser peUser = peUserService.findPeUserById(id);
        model.addAttribute("peUser",peUser);
        List<PeRole> peRoles = peRoleService.findAll();
        model.addAttribute("peRoles",peRoles);
        return "/pe/peUserManage/editPeUser";
    }

    @PostMapping("/pe/peUserManage/editPeUser")
    public String editPeUser(PeUser peUser, @RequestParam(name = "role",required = false)String[] s){
        List<PeRole> roleList = new ArrayList<>();
        if (s!=null){
            int[] a = new int[s.length];
            for (int i =0; i<s.length;i++){
                a[i] = Integer.parseInt(s[i]);
                roleList.add(peRoleService.findPeRoleById((long)a[i]));
            }
        }
        peUser.setPassword(peUserService.findPeUserById(peUser.getId()).getPassword());
        peUser.setRoles(roleList);
        peUser.setRolesName(peUser.getRolesNames());
        peUserService.editPeUser(peUser);
        return "/pe/peUserManage/queryPeUser";
    }

    @RequestMapping("/pe/peUserManage/toAddPeUser")
    public String toAddPeUser(Model model){
        List<PeRole> peRoles = peRoleService.findAll();
        model.addAttribute("peRoles",peRoles);
        return "/pe/peUserManage/addPeUser";
    }

    @PostMapping("/pe/peUserManage/addPeUser")
    public String addPeUser(PeUser peUser, @RequestParam(name = "role",required = false)String[] s){
        List<PeRole> roleList = new ArrayList<>();
        if (s!=null){
            int[] a = new int[s.length];
            for (int i =0; i<s.length;i++){
                a[i] = Integer.parseInt(s[i]);
                roleList.add(peRoleService.findPeRoleById((long)a[i]));
            }
        }
        if (peUser.getIdNumber().length()>=6){
            peUser.setPassword(peUser.getIdNumber().substring(peUser.getIdNumber().length()-6,peUser.getIdNumber().length()));
        }
        peUser.setRoles(roleList);
        peUser.setRolesName(peUser.getRolesNames());
        peUserService.savePeUser(peUser);
        return "/pe/peUserManage/queryPeUser";
    }

    @RequestMapping("/pe/peUserManage/toSqPeUser")
    public String toSqPeUser(Model model,Long id){
        PeUser peUser = peUserService.findPeUserById(id);
        model.addAttribute("peUser",peUser);
        List<PeRole> peRoles = peRoleService.findAll();
        model.addAttribute("peRoles",peRoles);
        return "/pe/peUserManage/sqPeUser";
    }
    @PostMapping("/pe/peUserManage/sqPeUser")
    public String sqPeUser(Model model,PeUser peUser, @RequestParam(name = "role",required = false)String[] s){
        List<PeRole> roleList = new ArrayList<>();
        if (s!=null){
            int[] a = new int[s.length];
            for (int i =0; i<s.length;i++){
                a[i] = Integer.parseInt(s[i]);
                roleList.add(peRoleService.findPeRoleById((long)a[i]));
            }
        }
        PeUser peUser1 = peUserService.findPeUserById(peUser.getId());
        peUser1.setRoles(roleList);
        peUser1.setRolesName(peUser1.getRolesNames());
        peUserService.savePeUser(peUser1);
        model.addAttribute("peUser",peUser1);
        return "/pe/peUserManage/queryPeUser";
    }
    @RequestMapping("/pe/peUserManage/toDeletePeUser")
    public String toDeletePeUser(Long id){
        peUserService.deletePeUserById(id);
        return "redirect:/pe/peUserManage/peUserList4";
    }

    @RequestMapping("/pe/peUserManage/toEditPassword")
    public String toEditPassword(Model model){
        PeUser peUser1 = (PeUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        PeUser peUser2 = peUserService.findPeUserByIdNumber(peUser1.getUsername());
        model.addAttribute("password",peUser2.getPassword());
        return "/pe/peUserManage/editPassword";
    }
    @RequestMapping("/pe/peUserManage/editPassword")
    public String editPassword(Model model, @RequestParam String npassword){
        PeUser peUser1 = (PeUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        PeUser peUser2 = peUserService.findPeUserByIdNumber(peUser1.getUsername());
        peUser2.setPassword(npassword);
        peUserService.editPeUser(peUser2);
        model.addAttribute("user",peUser2);
        return "/pe/index";
    }

    @RequestMapping("/pe/peUserManage/toEditPersonInfo")
    public String toEditPersonUser(Model model){
        PeUser peUser1 = (PeUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        PeUser peUser2 = peUserService.findPeUserByIdNumber(peUser1.getUsername());
        model.addAttribute("peUser",peUser2);
        return "/pe/peUserManage/queryPeUser";
    }
}
