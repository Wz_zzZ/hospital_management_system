package com.example.hospital.physicalExamination.service;


import com.example.hospital.physicalExamination.entity.PeItems;
import org.springframework.data.domain.Page;

import java.util.List;


public interface PeItemsService {

    public Page<PeItems> findItems(Integer page, Integer size, String id);
    public List<PeItems> findPeItemsByUserId(String userId);

    public PeItems findPeItemsById(Long id);

    public PeItems save(PeItems peItems);
    public void delete(Long id);
    public PeItems edit(PeItems peItems);


}
