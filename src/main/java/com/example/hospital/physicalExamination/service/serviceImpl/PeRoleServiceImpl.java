package com.example.hospital.physicalExamination.service.serviceImpl;

import com.example.hospital.physicalExamination.entity.PeRole;
import com.example.hospital.physicalExamination.repository.PeRoleRepository;
import com.example.hospital.physicalExamination.service.PeRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PeRoleServiceImpl implements PeRoleService {


    @Autowired
    PeRoleRepository peRoleRepository;

    public List<PeRole> findAll(){
        return peRoleRepository.findAll();
    }
    public PeRole findPeRoleById(Long id){
        return peRoleRepository.findPeRoleById(id);
    }
}
