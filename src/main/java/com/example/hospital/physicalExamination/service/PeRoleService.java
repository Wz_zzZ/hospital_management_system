package com.example.hospital.physicalExamination.service;

import com.example.hospital.physicalExamination.entity.PeRole;
import org.springframework.stereotype.Service;

import java.util.List;

public interface PeRoleService {

    public List<PeRole> findAll();
    public PeRole findPeRoleById(Long id);
}
