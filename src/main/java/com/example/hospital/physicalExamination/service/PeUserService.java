package com.example.hospital.physicalExamination.service;


import com.example.hospital.physicalExamination.entity.PeUser;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;


public interface PeUserService  {

    public Page<PeUser> findAll(Integer page, Integer size);
    public Page<PeUser> findPeUsers(Integer page, Integer size, PeUser peUser);

    public void savePeUser(PeUser peUser);
    public void editPeUser(PeUser peUser);
    public void deletePeUserById(Long id);
    public PeUser findPeUserByIdNumber(String idNumber);
    public PeUser findPeUserById(Long id);

}
