package com.example.hospital.physicalExamination.service.serviceImpl;

import com.example.hospital.physicalExamination.entity.PeUser;
import com.example.hospital.physicalExamination.repository.PeUserRepository;
import com.example.hospital.physicalExamination.service.PeUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

@Service
public class PeUserServiceImpl implements PeUserService {

    @Autowired
    PeUserRepository peUserRepository;

    //分页查询所有信息
    @Override
    public Page<PeUser> findAll(Integer page, Integer size) {
        Pageable pageable = new PageRequest(page, size, Sort.Direction.ASC, "id");
        return peUserRepository.findAll(pageable);
    }

    //分页模糊查询
    @Override
    public Page<PeUser> findPeUsers(Integer page, Integer size, PeUser peUser) {
        Pageable pageable = new PageRequest(page, size, Sort.Direction.ASC, "id");
        Page<PeUser> peUsers =  peUserRepository.findAll(new Specification<PeUser>() {
            @Override
            public Predicate toPredicate(Root<PeUser> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
                List<Predicate> list = new ArrayList<>();
                if(null != peUser.getIdNumber()&&!"".equals(peUser.getIdNumber())){
                    list.add(criteriaBuilder.like(root.get("idNumber").as(String.class), "%"+peUser.getIdNumber()+"%"));
                }
                if(null != peUser.getName()&&!"".equals(peUser.getName())){
                    list.add(criteriaBuilder.like(root.get("name").as(String.class), "%"+peUser.getName()+"%"));
                }
                Predicate[] p = new Predicate[list.size()];
                return criteriaBuilder.and(list.toArray(p));
            }
        },pageable);
        return peUsers;
    }

    @Override
    public void savePeUser(PeUser peUser) {
        peUserRepository.save(peUser);
    }

    @Override
    public void editPeUser(PeUser peUser) {
        peUserRepository.save(peUser);
    }

    @Override
    public void deletePeUserById(Long id) {
        peUserRepository.deleteById(id);
    }

    @Override
    public PeUser findPeUserByIdNumber(String idNumber) {
        PeUser peUser = peUserRepository.findPeUserByIdNumber(idNumber);
        return peUser;
    }
    public PeUser findPeUserById(Long id){
        return peUserRepository.findPeUserById(id);
    }
}
