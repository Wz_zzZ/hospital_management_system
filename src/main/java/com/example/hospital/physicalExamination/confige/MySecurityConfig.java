package com.example.hospital.physicalExamination.confige;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


@EnableWebSecurity
public class MySecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers("/static/css/**","/css/**","/static/js/**","/js/**").permitAll()
                .antMatchers("/pe/**").hasAnyRole("USER","ADMIN");
        //开启自动的配置功能 当没有登录页面，没有权限就会自动来到登录页面（自带）
        //.loginPage("/user/login")
        //http.formLogin().usernameParameter("username").passwordParameter("password").loginPage("/user/login");
        http.formLogin();
        //开启自动的注销功能
        http.logout().logoutSuccessUrl("/login");
        //开启记住我功能
        http.rememberMe().rememberMeParameter("remmenber");
        //关闭csrf
        http.csrf().disable();
        http.headers().frameOptions().disable();
    }
    //定义授权规则
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(myUserDetailsService).passwordEncoder(passwordEncoder());
    }



    @Autowired
    MyUserDetailsService myUserDetailsService;

    @Bean
    public MyPasswordEncoder passwordEncoder(){
        return new MyPasswordEncoder();
    }

}
