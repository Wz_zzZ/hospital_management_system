package com.example.hospital.physicalExamination.confige;

import com.example.hospital.physicalExamination.entity.PeUser;
import com.example.hospital.physicalExamination.repository.PeUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class MyUserDetailsService implements UserDetailsService {


    @Autowired
    PeUserRepository peUserRepository;
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        PeUser peUser = peUserRepository.findPeUserByIdNumber(s);
        if (peUser == null) {
            System.out.println("用户名不存在");
            throw new UsernameNotFoundException("用户名不存在");
        }
        return peUser;
    }
}
