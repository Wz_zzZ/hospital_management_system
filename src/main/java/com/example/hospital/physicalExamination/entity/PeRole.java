package com.example.hospital.physicalExamination.entity;

import javax.persistence.*;

@Entity
@Table(name = "peRole")
public class PeRole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name",length = 225)
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "PeRole{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
