package com.example.hospital.physicalExamination.entity;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "peUser")
public class PeUser implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;//id,主键
    @Column(name = "idNumber",unique = true)
    private String idNumber;//身份证号
    @Column(name = "password")
    private String password;//密码，默认身份证后六位
    @Column(name = "name")
    private String name;//姓名
    @Column(name = "age")
    private int age;
    @Column(name = "gender")
    private String gender;//性别
    @Column(name = "nation")
    private String nation;//民族
    @Column(name = "phone")
    private String phone;//电话
    @Column(name = "email")
    private String email;//邮箱
    @Column(name = "address")
    private String address;//地址
    @Column(name = "sid")
    private String sid;//学号
    @Column(name = "xueyuan")
    private String xueyuan;//学院
    @Column(name = "banji")
    private String banji;//班级

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getXueyuan() {
        return xueyuan;
    }

    public void setXueyuan(String xueyuan) {
        this.xueyuan = xueyuan;
    }

    public String getBanji() {
        return banji;
    }

    public void setBanji(String banji) {
        this.banji = banji;
    }

    @Override
    public String toString() {
        return "PeUser{" +
                "id=" + id +
                ", idNumber='" + idNumber + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", gender='" + gender + '\'' +
                ", nation='" + nation + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", address='" + address + '\'' +
                ", sid='" + sid + '\'' +
                ", xueyuan='" + xueyuan + '\'' +
                ", banji='" + banji + '\'' +
                '}';
    }

    @Column(name = "rolesname" )
    private String rolesName;

    public String getRolesName() {
        return rolesName;
    }

    public void setRolesName(String rolesName) {
        this.rolesName = rolesName;
    }

    public String getRolesNames(){
        String name="[";
        if (roles==null){
            name=name+"]";
        }else {
            int len = roles.size();
            for (PeRole peRole:roles) {
                if (len==1){
                    name=name+peRole.getName();
                }else {
                    name=name+peRole.getName()+",";
                }
                len--;
            }
            name=name+"]";
        }
        return name;
    }
    @ManyToMany(cascade = {CascadeType.REFRESH},fetch = FetchType.EAGER)
    @JoinTable(name="PeUserRole",
            joinColumns={@JoinColumn(name="PeUserId")},
            inverseJoinColumns={@JoinColumn(name="PeRoleId")}
    )
    private List<PeRole> roles;

    public List<PeRole> getRoles() {
        return roles;
    }

    public void setRoles(List<PeRole> roles) {
        this.roles = roles;
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> auths = new ArrayList<>();
        List<PeRole> roles = this.getRoles();
        for (PeRole role : roles) {
            auths.add(new SimpleGrantedAuthority(role.getName()));
        }
        return auths;
    }

    //返回验证用户密码,无法返回则NULL
    @Override
    public String getUsername() {
        return idNumber;
    }

    //账户是否过期,过期无法验证
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    //指定用户是否被锁定或者解锁,锁定的用户无法进行身份验证
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    //指示是否已过期的用户的凭据(密码),过期的凭据防止认证
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    //是否被禁用,禁用的用户不能身份验证
    @Override
    public boolean isEnabled() {
        return true;
    }
}
