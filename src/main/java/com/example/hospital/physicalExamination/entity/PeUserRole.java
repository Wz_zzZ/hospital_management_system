package com.example.hospital.physicalExamination.entity;

import javax.persistence.*;

@Entity
@Table(name = "PeUserRole")
public class PeUserRole {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private Long PeUserId;
    @Column
    private Long PeRoleId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPeUserId() {
        return PeUserId;
    }

    public void setPeUserId(Long peUserId) {
        PeUserId = peUserId;
    }

    public Long getPeRoleId() {
        return PeRoleId;
    }

    public void setPeRoleId(Long peRoleId) {
        PeRoleId = peRoleId;
    }

    @Override
    public String toString() {
        return "PeUserRole{" +
                "id=" + id +
                ", PeUserId=" + PeUserId +
                ", PeRoleId=" + PeRoleId +
                '}';
    }
}
