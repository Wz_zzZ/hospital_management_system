package com.example.hospital.physicalExamination.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "peItems")
public class PeItems {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    //用户id
    @Column(name = "userId")
    private String userId;
    //体检单号
    @Column(name = "peId" )
    private String peId;
    //试管编号
    @Column(name = "tubeId")
    private String tubeId;
    //左眼视力
    @Column(name = "visionLeft")
    private double visionLeft;
    //右眼视力
    @Column(name = "visionRight")
    private double visionRight;
    //色觉
    @Column(name = "colorVision")
    private String colorVision;
    //眼科检查员Ophthalmological inspector ID
    @Column(name = "oiId")
    private String oiID;
    @Column(name = "oiName")
    private String oiName;

    //血压
    @Column(name = "bloodPressure")
    private String bloodPressure;
    //心肺听诊Cardiopulmonary auscultation
    @Column(name = "ca")
    private String ca;
    //内科检查员 Medical Inspector
    @Column(name = "miId")
    private String miId;
    @Column(name = "miName")
    private String miName;

    //身高
    @Column(name = "height")
    private double height;
    //体重
    @Column(name = "weight")
    private double weight;
    //皮肤
    @Column(name = "skin")
    private String skin;
    //脊柱四肢Spinal limbs
    @Column(name = "spinalLimbs")
    private String spinalLimbs;
    //外科检查员Surgical inspector
    @Column(name = "siId")
    private String siId;
    @Column(name = "siName")
    private String siName;

    //血液
    @Column(name = "blood")
    private String blood;
    //血液检查员Blood inspector
    @Column(name = "biId")
    private String biId;
    @Column(name = "biName")
    private String biName;

    //胸部透析Chest dialysis
    @Column(name = "chestDialysis")
    private String chestDialysis;
    @Column(name = "cdiId")
    private String cdiId;
    @Column(name = "cdiName")
    private String cdiName;

    //创建时间
    @Column(name = "createDate")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createDate;
    @Column(name = "updateDate")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date updateDate;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPeId() {
        return peId;
    }

    public void setPeId(String peId) {
        this.peId = peId;
    }

    public String getTubeId() {
        return tubeId;
    }

    public void setTubeId(String tubeId) {
        this.tubeId = tubeId;
    }

    public double getVisionLeft() {
        return visionLeft;
    }

    public void setVisionLeft(double visionLeft) {
        this.visionLeft = visionLeft;
    }

    public double getVisionRight() {
        return visionRight;
    }

    public void setVisionRight(double visionRight) {
        this.visionRight = visionRight;
    }

    public String getColorVision() {
        return colorVision;
    }

    public void setColorVision(String colorVision) {
        this.colorVision = colorVision;
    }

    public String getOiID() {
        return oiID;
    }

    public void setOiID(String oiID) {
        this.oiID = oiID;
    }

    public String getOiName() {
        return oiName;
    }

    public void setOiName(String oiName) {
        this.oiName = oiName;
    }

    public String getBloodPressure() {
        return bloodPressure;
    }

    public void setBloodPressure(String bloodPressure) {
        this.bloodPressure = bloodPressure;
    }

    public String getCa() {
        return ca;
    }

    public void setCa(String ca) {
        this.ca = ca;
    }

    public String getMiId() {
        return miId;
    }

    public void setMiId(String miId) {
        this.miId = miId;
    }

    public String getMiName() {
        return miName;
    }

    public void setMiName(String miName) {
        this.miName = miName;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getSkin() {
        return skin;
    }

    public void setSkin(String skin) {
        this.skin = skin;
    }

    public String getSpinalLimbs() {
        return spinalLimbs;
    }

    public void setSpinalLimbs(String spinalLimbs) {
        this.spinalLimbs = spinalLimbs;
    }

    public String getSiId() {
        return siId;
    }

    public void setSiId(String siId) {
        this.siId = siId;
    }

    public String getSiName() {
        return siName;
    }

    public void setSiName(String siName) {
        this.siName = siName;
    }

    public String getBlood() {
        return blood;
    }

    public void setBlood(String blood) {
        this.blood = blood;
    }

    public String getBiId() {
        return biId;
    }

    public void setBiId(String biId) {
        this.biId = biId;
    }

    public String getBiName() {
        return biName;
    }

    public void setBiName(String biName) {
        this.biName = biName;
    }

    public String getChestDialysis() {
        return chestDialysis;
    }

    public void setChestDialysis(String chestDialysis) {
        this.chestDialysis = chestDialysis;
    }

    public String getCdiId() {
        return cdiId;
    }

    public void setCdiId(String cdiId) {
        this.cdiId = cdiId;
    }

    public String getCdiName() {
        return cdiName;
    }

    public void setCdiName(String cdiName) {
        this.cdiName = cdiName;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public String toString() {
        return "PeItems{" +
                "id=" + id +
                ", userId='" + userId + '\'' +
                ", peId='" + peId + '\'' +
                ", tubeId='" + tubeId + '\'' +
                ", visionLeft=" + visionLeft +
                ", visionRight=" + visionRight +
                ", colorVision='" + colorVision + '\'' +
                ", oiID='" + oiID + '\'' +
                ", oiName='" + oiName + '\'' +
                ", bloodPressure='" + bloodPressure + '\'' +
                ", ca='" + ca + '\'' +
                ", miId='" + miId + '\'' +
                ", miName='" + miName + '\'' +
                ", height=" + height +
                ", weight=" + weight +
                ", skin='" + skin + '\'' +
                ", spinalLimbs='" + spinalLimbs + '\'' +
                ", siId='" + siId + '\'' +
                ", siName='" + siName + '\'' +
                ", blood='" + blood + '\'' +
                ", biId='" + biId + '\'' +
                ", biName='" + biName + '\'' +
                ", chestDialysis='" + chestDialysis + '\'' +
                ", cdiId='" + cdiId + '\'' +
                ", cdiName='" + cdiName + '\'' +
                ", createDate=" + createDate +
                ", updateDate=" + updateDate +
                '}';
    }
}
