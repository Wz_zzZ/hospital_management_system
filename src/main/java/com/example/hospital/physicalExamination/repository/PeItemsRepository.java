package com.example.hospital.physicalExamination.repository;

import com.example.hospital.physicalExamination.entity.PeItems;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface PeItemsRepository extends JpaRepository<PeItems,Long>, JpaSpecificationExecutor<PeItems> {
    public List<PeItems> findPeItemsByUserId(String userId);
    public PeItems findPeItemsById(Long id);
}
