package com.example.hospital.physicalExamination.repository;

import com.example.hospital.physicalExamination.entity.PeUserRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PeUserRoleRepository extends JpaRepository<PeUserRole,Long> {
}
