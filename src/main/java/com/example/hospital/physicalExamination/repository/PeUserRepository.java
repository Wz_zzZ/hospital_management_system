package com.example.hospital.physicalExamination.repository;

import com.example.hospital.physicalExamination.entity.PeUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PeUserRepository extends JpaRepository<PeUser,Long>, JpaSpecificationExecutor<PeUser> {
    public PeUser findPeUserByIdNumber(String idNumber);
    public PeUser findPeUserById(Long id);
}
