package com.example.hospital.physicalExamination.repository;

import com.example.hospital.physicalExamination.entity.PeRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PeRoleRepository extends JpaRepository<PeRole,Long> {
    public PeRole findPeRoleById(Long id);
}
