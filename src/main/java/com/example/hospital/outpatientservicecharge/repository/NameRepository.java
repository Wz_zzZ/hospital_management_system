package com.example.hospital.outpatientservicecharge.repository;


import com.example.hospital.outpatientservicecharge.pojo.Name;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NameRepository extends JpaRepository<Name,String> {
}
