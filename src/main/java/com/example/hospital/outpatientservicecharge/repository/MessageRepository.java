package com.example.hospital.outpatientservicecharge.repository;

import com.example.hospital.outpatientservicecharge.pojo.Message;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepository extends JpaRepository<Message,String> {

    Message findByMid(String mid);
}
