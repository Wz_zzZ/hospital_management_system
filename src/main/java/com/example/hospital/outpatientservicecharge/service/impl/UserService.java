package com.example.hospital.outpatientservicecharge.service.impl;

import com.example.hospital.outpatientservicecharge.pojo.Message;

import java.util.List;

public class UserService {

    public interface Userservice {

        List<Message> getAllUser();

    }
}
