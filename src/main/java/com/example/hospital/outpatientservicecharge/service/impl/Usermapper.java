package com.example.hospital.outpatientservicecharge.service.impl;

import com.example.hospital.outpatientservicecharge.pojo.Message;
import org.springframework.stereotype.Repository;

import java.util.List;

public class Usermapper {
    @Repository
    public interface UserMapper {

        List<Message> getAllUser();
    }
}
