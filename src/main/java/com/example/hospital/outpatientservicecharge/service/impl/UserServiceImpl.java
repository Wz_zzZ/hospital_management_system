package com.example.hospital.outpatientservicecharge.service.impl;

import com.example.hospital.outpatientservicecharge.pojo.Message;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

public class UserServiceImpl {

    @Service(value = "userService")
    public class UserserviceImpl implements UserService.Userservice {

        @Resource
        private Usermapper.UserMapper userMapper;


        @Override
        public List<Message> getAllUser() {
            return userMapper.getAllUser();
        }
    }
}
