package com.example.hospital.outpatientservicecharge.pojo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "medicine1")
public class Medicine {
    @Id
    @GeneratedValue
    private int mId;
    private String name;
    private String units;
    private String specification;
    private float purchaseprice;
    private float saleprice;

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public float getPurchaseprice() {
        return purchaseprice;
    }

    public void setPurchaseprice(float purchaseprice) {
        this.purchaseprice = purchaseprice;
    }

    public float getSaleprice() {
        return saleprice;
    }

    public void setSaleprice(float saleprice) {
        this.saleprice = saleprice;
    }

    @Override
    public String toString() {
        return "Medicine{" +
                "mId=" + mId +
                ", name='" + name + '\'' +
                ", units='" + units + '\'' +
                ", specification='" + specification + '\'' +
                ", purchaseprice=" + purchaseprice +
                ", saleprice=" + saleprice +
                '}';
    }
}
