package com.example.hospital.outpatientservicecharge.pojo;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Entity
public class Name {
    @Id
    @GeneratedValue
    private String id;
    private String password;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Name{" +
                "id='" + id + '\'' +
                ", passwrod='" + password + '\'' +
                '}';
    }
}
