package com.example.hospital.outpatientservicecharge.pojo;

public class User {
    private String username;
    private String password;
    private String fname;
    private String sname;
    private String tname;
    private String tcost;
    private  int howhuch;

    public int getHowhuch() {
        return howhuch;
    }

    public void setHowhuch(int howhuch) {
        this.howhuch = howhuch;
    }

    public String getTname() {
        return tname;
    }

    public void setTname(String tname) {
        this.tname = tname;
    }

    public String getTcost() {
        return tcost;
    }

    public void setTcost(String tcost) {
        this.tcost = tcost;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
