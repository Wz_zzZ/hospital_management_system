package com.example.hospital.outpatientservicecharge.pojo;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;


@Entity
public class Message {

    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Id
    private String mid;
    private int allnum;
    private int lef;
    private int repay;
    private int lastpay;

    public int getLef() {
        return lef;
    }

    public void setLef(int lef) {
        this.lef = lef;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public int getAllnum() {
        return allnum;
    }

    public void setAllnum(int allnum) {
        this.allnum = allnum;
    }


    public int getRepay() {
        return repay;
    }

    public void setRepay(int repay) {
        this.repay = repay;
    }

    public int getLastpay() {
        return lastpay;
    }

    public void setLastpay(int lastpay) {
        this.lastpay = lastpay;
    }

    @Override
    public String toString() {
        return "Message{" +
                "mid='" + mid + '\'' +
                ", allnum=" + allnum +
                ", left=" + lef +
                ", repay=" + repay +
                ", lastpay=" + lastpay +
                '}';
    }
}
